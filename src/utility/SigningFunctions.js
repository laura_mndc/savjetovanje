import * as Routes from "../assets/constants/Paths";

export function logOut() {
  sessionStorage.clear();
  window.location.href = Routes.BASENAME;
}

function setUserSession(user) {
  sessionStorage.setItem("advisoringUserId", user._id);
  sessionStorage.setItem("advisoringUserName", user.name);
  sessionStorage.setItem("advisoringUserRole", user.role);
}

export function signUp(user) {
  setUserSession(user);
  window.location.href = Routes.BASENAME;
}

export function logIn(user) {
  setUserSession(user);
  window.location.href = Routes.BASENAME;
}

