import bcrypt from "bcryptjs";
import {
  Button,
  Form,
  FormGroup,
  FormControl,
  FormLabel,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState, useContext } from "react";
import { AuthContext } from "../contexts/auth-context.js";
import { signUp } from "../utility/SigningFunctions";
import {
  fetchUserByEmail,
  insertNewUser,
} from "../adapters/customAdapters/UserAdapter";

function RegisterPage() {
  
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [formIsInvalid, setFormIsInvalid] = useState("");
  const { getAccessToken } = useContext(AuthContext);

  async function insertUser() {
    let hash = bcrypt.hashSync(password, bcrypt.genSaltSync(8));
    let token = await getAccessToken("users");
    let role = document.getElementById("savjetnik").checked
      ? "savjetnik"
      : "brucos";
    let user = { email: email, name: name, role: role, hash: hash };
    const userExists=await fetchUserByEmail(email,token);
    if (!userExists) {
      let insertedUser = await insertNewUser(user, token);
      if (insertedUser) {
        signUp(insertedUser);
      } else throw new Error("Failed to insert data.");
    } else setFormIsInvalid("Račun s ovom e-mail adresom već postoji.");
  }

  function validateForm() {
    if (
      !(
        document.getElementById("savjetnik").checked ||
        document.getElementById("brucos").checked
      )
    ) {
      setFormIsInvalid("Označite ulogu.");
      return false;
    }
    if(!email.endsWith("@student.ferit.hr")){
      setFormIsInvalid("Neispravna e-mail adresa.");
      return false;
    }
    if (password.length < 6) {
      setFormIsInvalid("Lozinka je prekratka.");
      return false;
    }
    return true;
  }
  function handleSubmit(event) {
    event.preventDefault();

    if (validateForm()) {
      try {
        insertUser();
      } catch (error) {
        console.log(error.message);
      }
    }
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="Register container-md col-md-3 mt-5 p-4 rounded bg-light">
          <Form onSubmit={handleSubmit}>
            {formIsInvalid ? (
              <p className="text-danger">{formIsInvalid}</p>
            ) : (
              ""
            )}
            <FormGroup className="p-2" controlId="name">
              <FormLabel>Ime i prezime</FormLabel>
              <br></br>
              <FormControl
                required
                autoFocus
                value={name}
                placeholder="Ime Prezime"
                onChange={(e) => setName(e.target.value)}
              />
            </FormGroup>
            <FormGroup className="p-2" controlId="email">
              <FormLabel>E-mail</FormLabel>
              <br></br>
              <FormControl
                value={email}
                placeholder="ime.prezime@student.ferit.hr"
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup className="p-2" controlId="password">
              <FormLabel>Password</FormLabel>
              <br></br>
              <FormControl
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="******"
                type="password"
              />
            </FormGroup>

            <div className="mt-2 p-2">
              <fieldset>
                <Form.Group>
                  <Form.Check
                    inline
                    type="radio"
                    id="savjetnik"
                    name="uloga"
                    label="Savjetnik"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    id="brucos"
                    name="uloga"
                    label="Brucoš"
                  />
                </Form.Group>
              </fieldset>
            </div>

            <span className="p-2">
              Već imaš račun? <Link to="/">Prijavi se</Link>
            </span>

            <Button
              className="btn-secondary px-3 ms-auto d-block"
              block="true"
              type="submit"
            >
              Registracija
            </Button>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default RegisterPage;
