import bcrypt from "bcryptjs";
import {
  Button,
  Form,
  FormGroup,
  FormControl,
  FormLabel,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState, useContext } from "react";
import { AuthContext } from "../contexts/auth-context.js";
import { logIn } from "../utility/SigningFunctions";
import { fetchUserByEmail } from "../adapters/customAdapters/UserAdapter";

function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [authError, setAuthError] = useState(false);

  const { getAccessToken } = useContext(AuthContext);

  async function authorizeUser() {
    let token = await getAccessToken("users");
    let user = await fetchUserByEmail(email, token);

    if (
      user &&
      !user.deletedAccount &&
      bcrypt.compareSync(password, user.password)
    ) {
      logIn(user);
    } else setAuthError(true);
  }

  function handleSubmit(event) {
    event.preventDefault();
    authorizeUser();
  }
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="Login container-md col-md-3 mt-5 p-4 rounded bg-light">
          <Form onSubmit={handleSubmit}>
            {authError ? (
              <p className="text-danger">Neispravan e-mail ili lozinka.</p>
            ) : (
              ""
            )}
            <FormGroup className="p-2" controlId="email">
              <FormLabel>E-mail</FormLabel>
              <br></br>
              <FormControl
                autoFocus
                // type="email"
                value={email}
                placeholder="ime.prezime@student.ferit.hr"
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>
            <FormGroup className="p-2" controlId="password">
              <FormLabel>Password</FormLabel>
              <br></br>
              <FormControl
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="******"
                type="password"
              />
            </FormGroup>

            <span className="p-2">
              Nemaš račun? <Link to="/register">Registriraj se</Link>
            </span>

            <Button
              className="btn-secondary px-3 ms-auto d-block"
              block="true"
              type="submit"
            >
              Prijava
            </Button>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;
