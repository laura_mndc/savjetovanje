import React, { useState, useEffect, useContext } from "react";
import { CardGroup, Button } from "react-bootstrap";
import "../styles/profilepage-style.css";
import { AuthContext } from "../contexts/auth-context.js";
import AvailableStudentsCard from "../components/AvailableStudentsCard";
import AdvisementCard from "../components/AdvisementCard";
import UserData from "../components/UserData";
import { logOut } from "../utility/SigningFunctions";

import {
  fetchUserData,
  fetchAvailableStudents,
  updateUsersWithRemovedAdvisement,
  updateSentRequests,
  updateAfterRequestAcceptance,
  updateDeclinedRequest,
  updateDeletedUser,
} from "../adapters/customAdapters/UserAdapter";

function ProfilePage() {
  const loggedUserId = sessionStorage.getItem("advisoringUserId");
  const [queryError, setQueryError] = useState(false);
  const [state, setState] = useState({
    _id: loggedUserId,
    role: "",
    residence: "",
    study: "",
    education: "",
    dorm: false,
    knowledge: false,
    hobbies: [],
    advisements: [],
    sentAdvisementRequests: [],
  });
  const [availablePeople, setAvailablePeople] = useState([]);
  const { getAccessToken } = useContext(AuthContext);

  useEffect(() => {
    //sourceLogin definiran i unutar useEffect callbacka kao bi se izbjegao beskonacni re-render izazvan pozivom metode

    console.log("useEffect profile page pozvan ");

    async function setData() {
      const token = await getAccessToken("users");
      let user = await fetchUserData(loggedUserId, token);
      let nondeletedAdvisements;
      if (user) {
        //filtriranje studenata koji su obrisali profile
        if (user.advisements != null) {
          nondeletedAdvisements = user.advisements.filter(
            (user) => !user.deletedAccount
          );
        } else {
          nondeletedAdvisements = [];
        }
        user = {
          ...user,
          _id: loggedUserId,
          advisements: nondeletedAdvisements,
          sentAdvisementRequests: user.sentAdvisementRequests || [],
        };
        setState(user);
      } else setQueryError(true);

      //dohvacanje dostupnih studenata za izbor
      if (
        (user.role === "brucos" && nondeletedAdvisements.length === 0) ||
        (user.role === "savjetnik" && nondeletedAdvisements.length < 3) //provjera da student nema popunjenu kvotu
      ) {
        const availableStudents = await fetchAvailableStudents(user, token);
        if (availableStudents) {
          setAvailablePeople(() => [...availableStudents]);
        } else setQueryError(true);
      }
    }

    setData();
  }, [getAccessToken, loggedUserId]);

  async function removeAdvisement(userId) {
    const token = await getAccessToken("users");
    const newAdvisements = await updateUsersWithRemovedAdvisement(
      state.advisements,
      loggedUserId,
      userId,
      token
    );
    if (newAdvisements) {
      const updatedState = { ...state, advisements: newAdvisements };
      const availableStudents = await fetchAvailableStudents(
        updatedState,
        token
      );
      if (availableStudents) {
        setAvailablePeople(() => [...availableStudents]);
      }
      setState(updatedState);
    } else setQueryError(true);
  }

  async function sendRequest(advisorId) {
    state.sentAdvisementRequests.push(advisorId);
    let token = await getAccessToken("users");
    let result = await updateSentRequests(
      loggedUserId,
      state.sentAdvisementRequests,
      token
    );
    if (result) {
      setAvailablePeople(
        availablePeople.filter((item) => item._id !== advisorId)
      );
    } else setQueryError(true);
  }

  async function acceptRequest(freshmanUser) {
    //ukloniti zahtjev
    const filteredRequests = freshmanUser.sentAdvisementRequests.filter(
      (id) => id !== loggedUserId
    );
    freshmanUser = {
      ...freshmanUser,
      sentAdvisementRequests: filteredRequests,
    };
    //dodati u advisements oba korisnika
    freshmanUser.advisements.push(state);
    state.advisements.push(freshmanUser);

    let token = await getAccessToken("users");
    let result = await updateAfterRequestAcceptance(
      filteredRequests,
      freshmanUser,
      state,
      token
    );
    if (result) {
      setAvailablePeople(
        availablePeople.filter((item) => item._id !== freshmanUser._id)
      );
    } else setQueryError(true);
  }
  async function declineRequest(freshmanUser) {
    // samo maknuti iz zahtjeva

    const filteredRequests = freshmanUser.sentAdvisementRequests.filter(
      (id) => id !== loggedUserId
    );
    freshmanUser = {
      ...freshmanUser,
      sentAdvisementRequests: filteredRequests,
    };
    let token = await getAccessToken("users");
    let result = await updateDeclinedRequest(
      filteredRequests,
      freshmanUser._id,
      token
    );

    if (result) {
      setAvailablePeople(
        availablePeople.filter((item) => item._id !== freshmanUser._id)
      );
    } else setQueryError(true);
  }

  async function setUserDeleted() {
    let token = await getAccessToken("users");
    let result = await updateDeletedUser(loggedUserId, token);
    if (result) {
      logOut();
    } else setQueryError(true);
  }

  return (
    <div>
      <div className="col-md-8 border rounded m-4 p-4 bg-light">
        <div>
          <div className="header">
            {queryError ? <p>Neuspjeh u spajanju na bazu</p> : ""}
            <h4>Podaci</h4>
          </div>
          <UserData user={state} />
        </div>
      </div>
      <h4 className="bg-light d-inline-block px-3 py-1 rounded m-4">
        Dodijeljeni{" "}
        {state.role ? (state.role === "brucos" ? "savjetnik" : "studenti") : ""}
      </h4>
      <CardGroup className="d-flex flex-nowrap overflow-auto mt-auto ">
        {state.advisements.length > 0 ? (
          state.advisements.map((user, idx) => {
            return (
              <AdvisementCard
                user={user}
                key={idx}
                removeAdvisement={removeAdvisement}
              />
            );
          })
        ) : (
          <p className="bg-light d-inline-block px-3 py-1 rounded ms-4">
            Nemate dodijeljenih studenata.
          </p>
        )}
      </CardGroup>

      <h4 className="bg-light d-inline-block px-3 py-1 rounded m-4">
        Izbor{" "}
        {state.role
          ? state.role === "brucos"
            ? "savjetnika"
            : "studenata"
          : ""}
      </h4>

      <CardGroup className="d-flex flex-nowrap overflow-auto mt-auto">
        {state.residence ? (
          availablePeople.length > 0 ? (
            availablePeople.map((user, idx) => {
              return (
                <AvailableStudentsCard
                  user={user}
                  role={state.role}
                  key={idx}
                  sendRequest={sendRequest}
                  acceptRequest={acceptRequest}
                  declineRequest={declineRequest}
                />
              );
            })
          ) : (
            <p className="bg-light d-inline-block px-3 py-1 rounded ms-4">
              Kvota je popunjena ili trenutno nema dostupnih studenata.
            </p>
          )
        ) : (
          <p className="bg-light d-inline-block px-3 py-1 rounded ms-4">
            Upitnik nije ispunjen.
          </p>
        )}
      </CardGroup>

      <Button
        onClick={setUserDeleted}
        variant="outline-light"
        size="lg"
        className="ps-5 pe-5 m-5"
      >
        Obriši profil
      </Button>
    </div>
  );
}
export default ProfilePage;
