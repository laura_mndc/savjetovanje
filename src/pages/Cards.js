import React from "react";
import Card from "../components/CardUI";
import img_join from "../assets/images/advisor_1280.jpg";
import img_question from "../assets/images/question_1280.jpg";
import img_yell from "../assets/images/yell_1280.jpg";
import "../styles/card-style.css";
const Cards = () => {
  return (
    <div>
      <div className=" card-container container-fluid d-flex justify-content-center">
        <div className="row">
          <div className="col-md-4">
            <Card
              imgsrc={img_join}
              link="/advisorform"
              alt="3D character asking you to join him"
              title="Upitnik"
              description="Popuni upitnik o sebi za najdjelotvornije pronalaženje savjetnika. Ovdje možeš i prepraviti prethodno unesene podatke."
            />
          </div>
          <div className="col-md-4">
            <Card
              imgsrc={img_question}
              link="/faq"
              alt="3D character leaning on to a question mark"
              title="FAQ"
              description="Pronađi odgovore na najčešće postavljena pitanja o ovoj aplikaciji, pronalaženju savjetnika ili brucoša i fakultetskom životu."
            />
          </div>
          <div className="col-md-4">
            <Card
              imgsrc={img_yell}
              link="/forum"
              alt="3D character speaking into a megaphone"
              title="Forum"
              description="Pregledaj prethodno postavljena pitanja i komentare drugih korisnika aplikacije. Postavi vlastito pitanje ili komentar."
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Cards;
