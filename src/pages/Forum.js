import React, { useState, useEffect, useContext } from "react";
import { Button, Form } from "react-bootstrap";
import "../styles/forum-style.css";
import ForumTitle from "../components/ForumTitle";
import { AuthContext } from "../contexts/auth-context.js";
import {fetchForumTopics, insertNewTopic} from "../adapters/customAdapters/ForumAdapter";

function Forum() {

  const [forumTopics, setForumTopics] = useState([]);
  const [topicTitle, setTopicTitle] = useState("");
  const [topicDescription, setTopicDescription] = useState("");
  const { getAccessToken } = useContext(AuthContext);

  useEffect(() => {
    console.log("useEffect forum pozvan");
    async function fetchData() {
      let token = await getAccessToken("forum");
      let forumTopics = await fetchForumTopics(token);
      if (forumTopics) {
        setForumTopics(forumTopics);
      } else throw new Error("Failed to fetch data.");
    }
    try {
      fetchData();
    } catch (error) {
      console.log(error.message);
    }
  }, [getAccessToken]);

  async function addTopic(event) {
    event.preventDefault();
    let date = new Date();

    if (!forumTopics.some((e) => e.title === topicTitle.toUpperCase())) {
      
      const newTopic = {
        title: topicTitle.toUpperCase(),
        dateCreated: date.toLocaleString("hr-HR"),
        description: topicDescription,
        author: {
          userid: sessionStorage.getItem("advisoringUserId"),
          name: sessionStorage.getItem("advisoringUserName").split(" ")[0],
          role: sessionStorage.getItem("advisoringUserRole"),
        },
      };
      let token = await getAccessToken("forum");
      let querySuccessful = await insertNewTopic(newTopic, token);
      if (querySuccessful) {
        setTopicDescription("");
        setTopicTitle("");
        setForumTopics([...forumTopics, newTopic]);
        toggleNewTopic();
      } else throw new Error("Failed to add topic.");
    } else {
      alert("Već postoji tema s ovim naslovom.");
      setTopicTitle("");
      setTopicDescription("");
    }
  }
  function toggleNewTopic() {
    const classes = document.getElementById("newtopic-area").classList;
    if (classes.contains("hide")) {
      classes.remove("hide");
    } else {
      classes.add("hide");
    }
  }

  return (
    <div className="container mx-auto">
      <div className="subforum ">
        <div className="subforum-title">
          <h1>Teme</h1>
        </div>

        {forumTopics
          ? forumTopics.map((forum) => {
              return (
                <ForumTitle
                  title={forum.title}
                  description={forum.description}
                  numberOfComments={forum.comments ? forum.comments.length : 0}
                  lastCommenter={
                    forum.comments
                      ? forum.comments[forum.comments.length - 1].author.name
                      : ""
                  }
                  lastDate={
                    forum.comments
                      ? forum.comments[forum.comments.length - 1].date
                      : ""
                  }
                  path={forum.title.split(" ").join("-").toLowerCase()}
                  key={forum._id}
                />
              );
            })
          : ""}

        <div
          className="newtopic-area bg-white rounded ms-3 hide col-md-6"
          id="newtopic-area"
        >
          <Form className=" mt-3 p-2" onSubmit={addTopic}>
            <Form.Group className="col-md-4 mb-3">
              <Form.Label>Naslov</Form.Label>
              <Form.Control
                required
                type="text"
                value={topicTitle}
                onChange={(e) => setTopicTitle(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3 col-md-6">
              <Form.Label>Opis teme</Form.Label>
              <Form.Control
                required
                as="textarea"
                rows={3}
                name="newtopicdescription"
                value={topicDescription}
                onChange={(e) => setTopicDescription(e.target.value)}
              />
            </Form.Group>
            <Button
              variant="outline-dark"
              className="ms-auto  d-block"
              type="submit"
            >
              Dodaj temu
            </Button>
          </Form>
        </div>
        <div className="comment">
          <Button
            variant="outline-light"
            className=" ms-auto  d-block mt-2 me-2"
            onClick={toggleNewTopic}
          >
            Nova tema
          </Button>
        </div>
      </div>
    </div>
  );
}
export default Forum;