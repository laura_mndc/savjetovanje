import React, { useState, useEffect, useContext } from "react";
import { AuthContext } from "../contexts/auth-context.js";
import {
  fetchUserData,
  updateUserInfo,
} from "../adapters/customAdapters/UserAdapter";
import * as Routes from "../assets/constants/Paths";

function AdvisorForm() {
  const loggedUserId = sessionStorage.getItem("advisoringUserId");
  const [state, setState] = useState({
    id: loggedUserId,
    residence: "",
    study: "",
    education: "",
    dorm: false,
    knowledge: false,
    hobbies: [],
  });

  const { getAccessToken } = useContext(AuthContext);
  const [error, setError] = useState(false);
  const hobbies = [
    "Odbojka",
    "Nogomet",
    "Šah",
    "Biciklizam",
    "Fotografija",
    "Teretana",
    "Trčanje",
    "Gaming",
    "Sviranje instrumenta",
    "Čitanje",
    "Ples",
    "Rukomet",
    "Tenis",
  ];

  useEffect(() => {
    console.log("useEffect advisor form pozvan ");
    async function fetchData() {
      let token = await getAccessToken("users");
      //dohvacanje osobnih podataka
      let user = await fetchUserData(loggedUserId, token);
      if (user) {
        setState({
          _id: loggedUserId,
          residence: user.residence || "",
          hobbies: user.hobbies || [],
          study: user.study || "",
          education: user.education || "",
          dorm: user.dorm || false,
          knowledge: user.knowledge || false,
        });
      } else throw new Error("Failed to connect to database.");
    }

    try {
      fetchData();
    } catch (error) {
      console.log(error.message);
    }
  }, [loggedUserId, getAccessToken]);

  async function updateUser() {
    let token = await getAccessToken("users");
    let res = await updateUserInfo(state, token);

    if (res) {
      window.location.href = Routes.BASENAME;
    } else setError(true);
  }

  const hobbiesForHtml = hobbies.map((hobby) => {
    return { value: hobby, id: hobby, htmlFor: hobby };
  });
  const handleChange = (event) => {
    const value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;
    setState({
      ...state,
      [event.target.name]: value,
    });
  };

  const handleHobbiesChange = (event) => {
    const hobby = event.target.value.toLowerCase();
    const selectedCheckboxes = [...state.hobbies];
    const isChecked = selectedCheckboxes.includes(hobby);
    if (!isChecked) {
      selectedCheckboxes.push(hobby);
    } else {
      selectedCheckboxes.splice(selectedCheckboxes.indexOf(hobby),1);
    }

    setState({
      ...state,
      hobbies: selectedCheckboxes,
    });
  };
  function handleSubmit(event) {
    event.preventDefault();
    console.log(state);
    updateUser();
  }
  return (
    <div className="container-md mt-5 bg-light p-4 rounded">
      {error ? <p style={{ color: "red" }}>Invalid input.</p> : ""}
      <form className="row g-3" onSubmit={handleSubmit}>
        <div className="col-4">
          <label htmlFor="prebivaliste" className="form-label">
            Prebivalište
          </label>
          <input
            type="text"
            name="residence"
            className="form-control"
            id="prebivaliste"
            placeholder="Grad"
            value={state.residence}
            onChange={handleChange}
            required
          />
        </div>
        <br />
        <div className="col-md-4">
          <label htmlFor="inputState" className="form-label">
            Studij
          </label>
          <select
            name="study"
            id="inputState"
            className="form-select"
            value={state.study}
            onChange={handleChange}
            required
          >
            <option value="" disabled>
              Studiram...
            </option>
            <option value="Sveučilišni studij elektrotehnike">
              Sveučilišni studij elektrotehnike
            </option>
            <option value="Sveučilišni studij računarstva">
              Sveučilišni studij računarstva
            </option>
            <option value="Stručni studij elektrotehnike, smjer Automatika">
              {" "}
              Stručni studij elektrotehnike, smjer Automatika
            </option>
            <option value="Stručni studij elektrotehnike, smjer Elektroenergetika">
              Stručni studij elektrotehnike, smjer Elektroenergetika
            </option>
            <option value="Stručni studij računarstva">
              Stručni studij računarstva
            </option>
          </select>
        </div>
        <div className="col-md-4">
          <label htmlFor="inputState" className="form-label">
            Srednjoškolsko obrazovanje
          </label>
          <select
            name="education"
            id="inputState"
            className="form-select"
            value={state.education}
            onChange={handleChange}
            required
          >
            <option value="" disabled>
              Završio/la sam...
            </option>
            <option value="Gimnazija">Gimnaziju</option>
            <option value="Tehnička škola">Tehničku školu</option>
            <option value="Ostalo">Ostalo</option>
          </select>
        </div>
        <div className="col-12">
          <fieldset className="form-check">
            <legend>Hobiji:</legend>
            {hobbiesForHtml.map((hobby, idx) => {
              return (
                <div key={idx}>
                  <input
                    type="checkbox"
                    className="form-check-input"
                    name="hobi"
                    onChange={handleHobbiesChange}
                    checked={
                      state.hobbies
                        ? state.hobbies.includes(hobby.value.toLowerCase())
                        : false
                    }
                    value={hobby.value}
                    id={hobby.id}
                  />
                  <label htmlFor={hobby.htmlFor}>{hobby.value}</label>
                  <br />
                </div>
              );
            })}
            <br />
          </fieldset>
        </div>
        <div className="col-12">
          <div className="form-check">
            <input
              className="form-check-input"
              name="knowledge"
              checked={state.knowledge}
              onChange={handleChange}
              type="checkbox"
              id="gridCheck1"
            />
            <label className="form-check-label" htmlFor="gridCheck1">
              Na fakultet sam došao/la s predznanjem o struci
            </label>
          </div>
        </div>
        <div className="col-12">
          <div className="form-check">
            <input
              className="form-check-input"
              name="dorm"
              checked={state.dorm}
              onChange={handleChange}
              type="checkbox"
              id="gridCheck2"
            />
            <label className="form-check-label" htmlFor="gridCheck2">
              Živim u studentskom domu
            </label>
          </div>
        </div>
        <div className="col-12">
          <button type="submit" className="btn btn-outline-dark">
            Prijavi se
          </button>
        </div>
      </form>
    </div>
  );
}

export default AdvisorForm;
