import React, { useState, useContext, useEffect } from "react";
import { Accordion } from "react-bootstrap";
import AccordionQuestion from "../components/AccordionQuestion";
import { AuthContext } from "../contexts/auth-context.js";
import {fetchFAQ} from "../adapters/customAdapters/FAQAdapter";

function FAQ() {

  const [FAQs, setFAQs] = useState([]);
  const { getAccessToken } = useContext(AuthContext);

  useEffect(() => {
    console.log("useEffect faq pozvan");

    async function fetchQuestions() {
      let token = await getAccessToken("faq");
      let queryResult = await fetchFAQ(token);
      if (queryResult) {
        setFAQs(queryResult);
      } else throw new Error("Failed to fetch data.");
    }
    try {
      fetchQuestions();
    } catch (error) {
      console.log(error.message);
    }
  }, [getAccessToken]);
  return (
    <div className="container">
      <Accordion>
        {FAQs
          ? FAQs.map((question, idx) => (
              <AccordionQuestion
                question={question.question}
                answer={question.answer}
                key={idx}
                index={idx}
              />
            ))
          : ""}
      </Accordion>
    </div>
  );
}

export default FAQ;

