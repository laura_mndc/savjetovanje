import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { AuthContext } from "../contexts/auth-context.js";
import ForumComment from "../components/ForumComment";
import {
  fetchOneForumTopic,
  insertNewComment,
} from "../adapters/customAdapters/ForumAdapter";

export default function ForumTopic(props) {
  const { getAccessToken } = useContext(AuthContext);
  const [forumTopic, setForumTopic] = useState({});
  const [comment, setComment] = useState("");
  let path = props.match.params.path;
  let titleUpperCase = path.split("-").join(" ").toUpperCase();

  useEffect(() => {
    console.log("useEffect forumtopic pozvan");
    async function fetchData() {
      let token = await getAccessToken("forum");
      let topic = await fetchOneForumTopic(titleUpperCase, token);
      if (topic) {
        setForumTopic(topic);
      } else throw new Error("Failed to fetch data.");
    }
    fetchData();
  }, [titleUpperCase, getAccessToken]);

  async function addReply() {
    let date = new Date();
    let updatedComments;
    let newComment = {
      content: comment,
      date: date.toLocaleString("hr-HR"),
      author: {
        userid: sessionStorage.getItem("advisoringUserId"),
        name: sessionStorage.getItem("advisoringUserName").split(" ")[0],
        role: sessionStorage.getItem("advisoringUserRole"),
      },
    };
    if (forumTopic.comments) {
      updatedComments = [...forumTopic.comments, newComment];
    } else {
      updatedComments = [newComment];
    }

    let token = await getAccessToken("forum");
    let commentInserted = await insertNewComment(
      updatedComments,
      titleUpperCase,
      token
    );
    if (commentInserted) {
      console.log("Inserted comments");
      setForumTopic({ ...forumTopic, comments: updatedComments });
      setComment("");
      toggleReply();
    } else throw new Error("Failed to update data.");
  }

  function toggleReply() {
    const classes = document.getElementById("reply-area").classList;
    if (classes.contains("hide")) {
      classes.remove("hide");
    } else {
      classes.add("hide");
    }
  }

  return (
    <div className="container mx-auto">
      <div className="navigate">
        <span>
          <Link to="/forum">Forum</Link> -{" "}
          <Link to={`/forum/${props.match.params.path}`}>
            {forumTopic.title}
          </Link>
        </span>
      </div>

      <div className="head">
        <div className="authors">
          Autor: {forumTopic.author ? forumTopic.author.name : ""}
        </div>
        <div className="content">Tema: {forumTopic.title}</div>
      </div>

      {forumTopic.comments
        ? forumTopic.comments.map((comment, idx) => {
            return (
              <ForumComment
                name={comment.author.name}
                role={comment.author.role}
                date={comment.date}
                content={comment.content}
                key={idx}
              />
            );
          })
        : ""}

      <div className="comment-area hide" id="reply-area">
        <textarea
          name="reply"
          value={comment}
          id=""
          placeholder="Napiši komentar... "
          onChange={(e) => setComment(e.target.value)}
        ></textarea>
        <Button
          variant="outline-light"
          className="ms-auto  d-block mt-2"
          onClick={addReply}
        >
          Komentiraj
        </Button>
      </div>
      <div className="comment">
        <Button variant="outline-light" className=" mt-4" onClick={toggleReply}>
          Novi komentar
        </Button>
      </div>
    </div>
  );
}
