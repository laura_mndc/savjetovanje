import axios from "axios";
import { mongoQuery } from "../xhr/QueryMongo";
const sRealmApp = "users-cntxf";
const CancelTokenLogin = axios.CancelToken;
const sourceLogin = CancelTokenLogin.source();

export async function fetchUserData(userId, token) {
  //dohvacanje osobnih podataka
  const userQuery = `query {user (query:{_id:"${userId}"}) {name, role, residence, study, education, hobbies, dorm, knowledge, advisements{_id, email, name, residence, study, education, hobbies, dorm, deletedAccount, knowledge, advisements{_id} }, sentAdvisementRequests }}`;
  let queryResult = await mongoQuery(token, sRealmApp, userQuery, sourceLogin);
  if (queryResult.data.data !== null && queryResult.data.data.user !== null) {
    return queryResult.data.data.user;
  } else return false;
}

export async function fetchAvailableStudents(user, token) {
  let sQueryAvailablePeople;
  let nonavailablePeopleJoined;
  if (user.role === "brucos") {
    const nonavailablePeople = user.sentAdvisementRequests;
    nonavailablePeopleJoined = nonavailablePeople
      .map((person) => `"${person}"`)
      .join(",");

    //kupim sve dostupne savjetnike kojima je deletedaccount false ili null
    sQueryAvailablePeople = `query {users (query:{role:"savjetnik",deletedAccount_ne:true,_id_nin:[${nonavailablePeopleJoined}]})
        {_id, name, email, residence, study, education, hobbies, dorm, knowledge,advisements {_id} }}`;
  } else {
    //ako je savjetnik
    //kupim sve brucose koji su ulogiranom savjetniku poslali zahtjev kojima je deletedaccount false ili null
    const loggedUserId = sessionStorage.getItem("advisoringUserId");
    sQueryAvailablePeople = `query {users(query:{role:"brucos",deletedAccount_ne:true,sentAdvisementRequests_in:["${loggedUserId}"]}) { _id, name, email, residence, study, education, hobbies, dorm, knowledge, advisements{_id}, sentAdvisementRequests} }`;
  }

  let queryResultAvailablePeople = await mongoQuery(
    token,
    sRealmApp,
    sQueryAvailablePeople,
    sourceLogin
  );
  if (
    queryResultAvailablePeople.data.data != null &&
    queryResultAvailablePeople.data.data.users != null
  ) {
    const result = queryResultAvailablePeople.data.data.users;

    if (user.role === "brucos") {
      //ako je ulogirani student brucos, ne moze izabrati savjetnika s 3 ili vise savjetovanja
      result.filter((item) => item.advisements.length >= 3);
    } else {
      //savjetnik ne moze izabrati studenta koji vec ima savjetnika
      result.filter((item) => item.advisements.length >= 1);
    }
    return result;
  } else return false;
}

export async function updateUsersWithRemovedAdvisement(
  userAdvisements,
  loggedUserId,
  removedUserId,
  token
) {
  //sacuvati samo objekte ciji id nije removedUserId
  const loggedUserAdvisementsFiltered = userAdvisements.filter(
    (user) => user._id !== removedUserId
  );
  //izvlacenje id-a iz polja objekata i spremanje u array
  const loggedUserAdvisementIdsFiltered = loggedUserAdvisementsFiltered.map(
    (user) => user._id
  );
  //find vraca objekt uklonjenog korisnika
  const removedUser = userAdvisements.find(
    (user) => user._id === removedUserId
  );
  //advisements sadrzi polje objekata u kojem se cuvaju svi objekti osim prijavljenog korisnika
  const removedUserAdvisementsFiltered = removedUser.advisements.filter(
    (user) => user._id !== loggedUserId
  );
  //pretvaranje u array id-eva
  const removedUserAdvisementIdsFiltered = removedUserAdvisementsFiltered.map(
    (user) => user._id
  );

  //pretvaranje u format koji se moze poslati http requestom
  const advisementsJoined = loggedUserAdvisementIdsFiltered
    .map((advisement) => `"${advisement}"`)
    .join(",");
  const advisementsJoinedSecond = removedUserAdvisementIdsFiltered
    .map((advisement) => `"${advisement}"`)
    .join(",");

  const queryLoggedUser = `mutation{updateOneUser(query: {_id:"${loggedUserId}"},set:{ advisements:{link:[${advisementsJoined}] }}){advisements{_id}}}`;
  const querySecondUser = `mutation{updateOneUser(query: {_id:"${removedUserId}"},set:{ advisements:{link:[${advisementsJoinedSecond}] }}){advisements{_id}}}`;

  let queryResult1 = await mongoQuery(
    token,
    sRealmApp,
    queryLoggedUser,
    sourceLogin
  );
  let queryResult2 = await mongoQuery(
    token,
    sRealmApp,
    querySecondUser,
    sourceLogin
  );
  if (
    queryResult1.data.data.updateOneUser !== null &&
    queryResult2.data.data.updateOneUser !== null
  ) {
    return loggedUserAdvisementsFiltered;
  } else return false;
}

export async function updateSentRequests(userId, newSentRequests, token) {
  const sentRequestsJoined = newSentRequests
    .map((request) => `"${request}"`)
    .join(",");

  const queryLoggedUser = `mutation{updateOneUser(query: {_id:"${userId}"},set:{ sentAdvisementRequests:[${sentRequestsJoined}] }){sentAdvisementRequests}}`;

  let queryResult = await mongoQuery(
    token,
    sRealmApp,
    queryLoggedUser,
    sourceLogin
  );
  if (queryResult.data.data.updateOneUser !== null) {
    return true;
  } else return false;
}
export async function updateAfterRequestAcceptance(
  filteredRequests,
  freshmanUser,
  loggedUser,
  token
) {
  //formatirati za graphql query
  const sentRequestsJoined = filteredRequests
    .map((request) => `"${request}"`)
    .join(",");

  const freshmanAdvisementsJoined = freshmanUser.advisements
    .map((user) => `"${user._id}"`)
    .join(",");
  const advisorAdvisementsJoined = loggedUser.advisements
    .map((user) => `"${user._id}"`)
    .join(",");

  const queryFreshman = `mutation{updateOneUser(query: {_id:"${freshmanUser._id}"},set:{ sentAdvisementRequests:[${sentRequestsJoined}], advisements:{link: [${freshmanAdvisementsJoined}] }}){sentAdvisementRequests}}`;
  const queryAdvisor = `mutation{updateOneUser(query: {_id:"${loggedUser._id}"},set:{ advisements:{link: [${advisorAdvisementsJoined}]  }}){advisements{_id}}}`;

  let queryResultFreshman = await mongoQuery(
    token,
    sRealmApp,
    queryFreshman,
    sourceLogin
  );
  let queryResultAdvisor = await mongoQuery(
    token,
    sRealmApp,
    queryAdvisor,
    sourceLogin
  );
  if (
    queryResultFreshman.data.data.updateOneUser != null &&
    queryResultAdvisor.data.data.updateOneUser != null
  ) {
    return true;
  } else return false;
}

export async function updateDeclinedRequest(
  filteredRequests,
  freshmanUserId,
  token
) {
  const sentRequestsJoined = filteredRequests
    .map((request) => `"${request}"`)
    .join(",");

  const queryFreshman = `mutation{updateOneUser(query: {_id:"${freshmanUserId}"},set:{ sentAdvisementRequests:[${sentRequestsJoined}]}){sentAdvisementRequests}}`;

  let queryResult = await mongoQuery(
    token,
    sRealmApp,
    queryFreshman,
    sourceLogin
  );
  if (queryResult.data.data.updateOneUser != null) {
    return true;
  } else return false;
}

export async function updateDeletedUser(loggedUserId, token) {
  const queryLoggedUser = `mutation{updateOneUser(query: {_id:"${loggedUserId}"},set:{ advisements:{link: []}, deletedAccount:true }){deletedAccount}}`;
  let queryResult = await mongoQuery(
    token,
    sRealmApp,
    queryLoggedUser,
    sourceLogin
  );
  if (queryResult.data.data.updateOneUser !== null) {
    return true;
  } else {
    return false;
  }
}
export async function fetchUserByEmail(email, token) {
  const sLoginQuery = `query {user (query:{email:"${email}"}) {_id, name, role, password, deletedAccount } }`;
  let res = await mongoQuery(token, sRealmApp, sLoginQuery, sourceLogin);
  if (res.data.data !== null) {
    return res.data.data.user;
  } else return false;
}

export async function insertNewUser(user, token) {
  let sQuery = `mutation {insertOneUser(data: {email: "${user.email}", name: "${user.name}", password: "${user.hash}", role: "${user.role}"}) {_id, name, role} }`;
  let res = await mongoQuery(token, sRealmApp, sQuery, sourceLogin);
  if (res.data.data !== null) {
    return res.data.data.insertOneUser;
  } else {
    return false;
  }
}
export async function updateUserInfo(user, token) {
  const hobbiesJoined = user.hobbies.map((hobby) => `"${hobby}"`).join(",");
  const sQuery = `mutation{updateOneUser(query: {_id:"${user._id}"},set:{dorm:${user.dorm}, residence: "${user.residence}", study: "${user.study}", education: "${user.education}", knowledge: ${user.knowledge}, hobbies:[${hobbiesJoined}] }){residence}}`;
  let res = await mongoQuery(token, sRealmApp, sQuery, sourceLogin);
  if (res.data.data != null && res.data.data.updateOneUser != null) {
    return true;
  } else return false;
}
