import axios from "axios";
import { mongoQuery } from "../xhr/QueryMongo";
const sRealmApp = "forumtopics-roiur";
const CancelTokenLogin = axios.CancelToken;
const sourceLogin = CancelTokenLogin.source();

export async function fetchForumTopics(token) {
  const query = `query {forumTopics { _id, comments {content date author{name, role}} dateCreated, description title } }`;
  let queryResult = await mongoQuery(token, sRealmApp, query, sourceLogin);
  if (queryResult.data.data !== null) {
    return queryResult.data.data.forumTopics;
  } else return false;
}

export async function insertNewTopic(newTopic, token) {
  const newTopicFormatted = JSON.stringify(newTopic).replace(
    /"([^"]+)":/g,
    "$1:"
  );
  const sQuery = `mutation {insertOneForumTopic(data:${newTopicFormatted}) { _id, dateCreated, description, title } }`;

  let queryResult = await mongoQuery(token, sRealmApp, sQuery, sourceLogin);
  if (
    queryResult.data.data !== null &&
    queryResult.data.data.insertOneForumTopic !== null
  ) {
    return true;
  } else return false;
}

export async function fetchOneForumTopic(titleUpperCase, token) {
  const sQuery = `query {forumTopic(query:{title:"${titleUpperCase}"}) {title,description,dateCreated, author{ name, role },comments{author{name, role} date content} }}`;

  let queryResult = await mongoQuery(token, sRealmApp, sQuery, sourceLogin);
  if (queryResult.data.data != null) {
    return queryResult.data.data.forumTopic;
  } else return false;
}

export async function insertNewComment(updatedComments, titleUpperCase, token) {
  const comments = JSON.stringify(updatedComments).replace(
    /"([^"]+)":/g,
    "$1:"
  );
  const sQuery = `mutation {updateOneForumTopic(query:{title:"${titleUpperCase}"},set:{comments:${comments}}) { _id } }`;

  let queryResult = await mongoQuery(token, sRealmApp, sQuery, sourceLogin);
  if (
    queryResult.data.data !== null &&
    queryResult.data.data.updateOneForumTopic !== null
  ) {
    return true;
  } else return false;
}
