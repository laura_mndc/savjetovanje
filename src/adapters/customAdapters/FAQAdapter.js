import axios from "axios";
import { mongoQuery } from "../xhr/QueryMongo";
const sRealmApp = "faq-rwfci";
const CancelTokenLogin = axios.CancelToken;
const sourceLogin = CancelTokenLogin.source();

export async function fetchFAQ(token) {
  const sQuery = `query {fAQS { answer question } }`;
  let queryResult = await mongoQuery(token, sRealmApp, sQuery, sourceLogin);
  if (queryResult.data.data !== null) {
    return queryResult.data.data.fAQS;
  } else return false;
}
