import axios from "axios";

export function mongoLogin(realmApp) {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `https://eu-central-1.aws.realm.mongodb.com/api/client/v2.0/app/${realmApp}/auth/providers/anon-user/login`,
        {},
        { headers: { "Content-Type": "application/json" } }
      )
      .then((res) => {
        resolve(res);
      });
  });
}

export function mongoQuery(access_token, realmApp, query, source) {
  return new Promise((resolve, reject) => {
    var endpoint = `https://eu-central-1.aws.realm.mongodb.com/api/client/v2.0/app/${realmApp}/graphql`;
    var sToken = "Bearer " + access_token;
    axios
      .post(
        endpoint,
        { query: query },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: sToken,
          },
          cancelToken: source.token,
        }
      )
      .then((res) => {
        console.log("Mongoquery res:", res);
        resolve(res);
      })
      .catch((rejectReason) => console.log(rejectReason));
  });
}
