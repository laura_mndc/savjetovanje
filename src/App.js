import React from "react";
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Cards from "./pages/Cards";
import AdvisorForm from "./pages/AdvisorForm";
import CustomNavbar from "./components/CustomNavbar";
import Forum from "./pages/Forum";
import ForumTopic from "./pages/ForumTopic";
import LoginPage from "./pages/LoginPage.js";
import RegisterPage from "./pages/RegisterPage";
import ProfilePage from "./pages/ProfilePage";
import FAQ from "./pages/FAQ";
import * as Routes from "./assets/constants/Paths";

function App() {
  let id=sessionStorage.getItem("advisoringUserId");
  if (id) {
    return (
  
        <Router basename={Routes.BASENAME}>
          <Route path="/" component={CustomNavbar} />
          <Route exact path={Routes.HOME} component={Cards} />
          <Route exact path={Routes.ADVISOR_FORM} component={AdvisorForm} />
          <Route exact path={Routes.PROFILE} component={ProfilePage} />
          <Route exact path={Routes.FAQ} component={FAQ} />
          <Route exact path={Routes.FORUM} component={Forum} />
          <Route exact path={Routes.FORUM_TOPIC} component={ForumTopic} />
        </Router>
      
    );
  } else {
    return (
      <Router basename={Routes.BASENAME}>
        <Route exact path={Routes.HOME} component={LoginPage} />
        <Route exact path={Routes.REGISTER} component={RegisterPage}/>
      </Router>
    );
  }
}

export default App;
