import React from "react";
import { Card, Button } from "react-bootstrap";

function AdvisementCard(props) {
  return (
    <Card className="m-4 p-3 rounded bg-light">
      <Card.Body className="d-flex flex-column">
        <Card.Title>{props.user.name}</Card.Title>
        <Card.Text className="text-primary">
          E-mail: {props.user.email}
        </Card.Text>
        <Card.Text>Prebivalište: {props.user.residence}</Card.Text>
        <Card.Text>Studij: {props.user.study}</Card.Text>
        <Card.Text>Školovanje: {props.user.education}</Card.Text>
        <Card.Text>
          Hobiji: {props.user.hobbies ? props.user.hobbies.join(", ") : ""}
        </Card.Text>
        {props.user.dorm ? <Card.Text>Živi u studentskom domu</Card.Text> : ""}
        {props.user.knowledge ? (
          <Card.Text>Došao/la na fakultet s predznanjem</Card.Text>
        ) : (
          ""
        )}

        <Button
          className="col-sm-5 mt-auto align-self-center"
          variant="outline-danger"
          onClick={() => {
            props.removeAdvisement(props.user._id);
          }}
        >
          Ukloni
        </Button>
      </Card.Body>
    </Card>
  );
}

export default AdvisementCard;
