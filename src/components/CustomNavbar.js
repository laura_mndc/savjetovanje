import {Navbar,Nav,Button} from 'react-bootstrap';
import { Link} from "react-router-dom";
import "../styles/navbar-style.css"
import {logOut} from "../utility/SigningFunctions";
import * as Routes from "../assets/constants/Paths";

export default function CustomNavbar(){

    return(
      <div className="container-fluid">
        <Navbar className="row">
            
        <Nav className="d-flex col-md-11 justify-content-around">
          <Link className="p-2" to={Routes.HOME}>Početna</Link>
          <Link className="p-2" to={Routes.ADVISOR_FORM}> Upitnik </Link>
          <Link className="p-2" to={Routes.FAQ}>FAQ</Link>
          <Link className="p-2" to={Routes.FORUM}>Forum </Link>
          <Link className="p-2" to={Routes.PROFILE}>Moj profil</Link>
        </Nav>
        <Nav className="p-2 col-md-1">
        <Button variant="outline-light" className="ms-auto" onClick={logOut}>Odjava</Button>
        </Nav>
      </Navbar>
      </div>
      
    );

}
