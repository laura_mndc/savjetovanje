import React from "react";
import { Link } from "react-router-dom";

function ForumTitle(props) {
  
  return (
    <div className="subforum-row">
      <div className="subforum-description subforum-column">
        <h4>
          <Link to={`/forum/${props.path}`}>{props.title}</Link>
        </h4>
        <p>
          {props.description}{" "}
        </p>
      </div>
      <div className="subforum-stats subforum-column center">
        <span>{props.numberOfComments} komentara</span>
      </div>
      <div className="subforum-info subforum-column">
        
         {props.numberOfComments!==0 ? (<p>Zadnji komentirao/la {props.lastCommenter}
        <br />
         <small>dana {props.lastDate}</small></p>): ("Ova tema nema komentara")}
      </div>
    </div>
  );
}
export default ForumTitle;
