import React from "react";
import { Accordion } from "react-bootstrap";
export default function AccordionQuestion(props) {
  return (
    <Accordion.Item eventKey={props.index}>
      <Accordion.Header>{props.question}</Accordion.Header>
      <Accordion.Body>{props.answer}</Accordion.Body>
    </Accordion.Item>
  );
}
