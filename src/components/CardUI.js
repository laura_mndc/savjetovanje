import React from 'react';
import {
 
    Link
  } from "react-router-dom";
const Card= props =>{
    return(
        <Link to={props.link}>
            <div className="cardui text-center shadow">
                <div className="overflow">
                    <img src={props.imgsrc} alt={props.alt}className="card-img-top"/>
                </div>
                <div className="cardui-body text-dark">
                    <h4 className="card-title">{props.title}</h4>
                    <p className="cardui-text text-secondary">
                        {props.description}
                    </p>
                    
                </div>
            </div>
        </Link>
    );
}
export default Card;