import React from 'react';
import {Card, Button, ButtonGroup} from "react-bootstrap";

function AvailableStudentsCard(props) {
    return (
        <Card className="m-4 p-3 rounded bg-light">
                  <Card.Body className="d-flex flex-column">
                    <Card.Title>{props.user.name.split(" ")[0]}</Card.Title>
                    <Card.Text>Prebivalište: {props.user.residence}</Card.Text>
                    <Card.Text>Studij: {props.user.study}</Card.Text>
                    <Card.Text>
                      Prethodno školovanje: {props.user.education}
                    </Card.Text>
                    <Card.Text>Hobiji: {props.user.hobbies?props.user.hobbies.join(", "): ""}</Card.Text>
                    {props.user.dorm ? (
                      <Card.Text>Živi u studentskom domu</Card.Text>
                    ) : (
                      ""
                    )}
                    {props.user.knowledge ? (
                      <Card.Text>Došao/la na fakultet s predznanjem</Card.Text>
                    ) : (
                      ""
                    )}

                    {props.role === "brucos" ? (
                      <Button
                        className="col-sm-5 mt-auto align-self-center"
                        variant="outline-success"
                        onClick={() => {
                        props.sendRequest(props.user._id);
                        }}
                      >
                        Dodaj
                      </Button>
                    ) : (
                      <ButtonGroup>
                        <Button
                          className="col-sm-5 mt-auto me-1 align-self-center rounded"
                          variant="outline-success"
                          onClick={() => {
                            props.acceptRequest(props.user);
                          }}
                        >
                          Prihvati
                        </Button>
                        <Button
                          className="col-sm-5 mt-auto ms-1 align-self-center rounded "
                          variant="outline-danger"
                          onClick={() => {
                            props.declineRequest(props.user);
                          }}
                        >
                          Odbij
                        </Button>
                      </ButtonGroup>
                    )}
                  </Card.Body>
                </Card>
    )
}

export default AvailableStudentsCard;
