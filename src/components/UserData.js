import React from 'react'

function UserData(props) {
    return (
        <div className="  kocka-body">
          <p className="fw-bold m-0">Ime i prezime</p>
        <p>{props.user.name}</p>
        <p className="fw-bold m-0">Uloga</p>
        <p>{props.user.role==="savjetnik"?"Savjetnik": "Brucoš"}</p>
        <p className="fw-bold m-0">Prebivalište</p>
        <p>{props.user.residence}</p>
        <p className="fw-bold m-0">Studij</p>
        <p>{props.user.study}</p>
        <p className="fw-bold m-0">Prethodno školovanje</p>
        <p>{props.user.education}</p>
        <p className="fw-bold m-0">Hobiji</p>
        <p>
          {props.user.hobbies
            ? props.user.hobbies.join(", ").charAt(0).toUpperCase() +
            props.user.hobbies.join(", ").slice(1)
            : ""}
        </p>
        <br />
        <p className=" ">
          Živim u studentskom domu: {props.user.dorm ? "DA" : "NE"}
        </p>
        <p className=" ">
          Na fakultet dolazim s predznanjem: {props.user.knowledge ? "DA" : "NE"}
        </p>
      </div>
    )
}

export default UserData
