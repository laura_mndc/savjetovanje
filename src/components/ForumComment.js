import React from "react";

function ForumComment(props) {
  

  return (
    <div>
      <div className="comments-container">
        <div className="body">
          <div className="authors">
            <div className="username">{props.name}</div>
            <div>{props.role==="savjetnik"?"Savjetnik":"Brucoš"}</div>
            <div>{props.date}</div>
          </div>
          <div className="content">
            {props.content}
           
          </div>
        </div>
      </div>

    
    </div>
  );
}

export default ForumComment;
