import React, { useState } from "react";
import { mongoLogin } from "../adapters/xhr/QueryMongo";
const AuthContext = React.createContext();

const AuthProvider = ({ children }) => {
  const [accTokens, setAccTokens] = useState({
    forumAccToken: { value: "", time: null },
    faqAccToken: { value: "", time: null },
    usersAccToken: { value: "", time: null },
  });

  async function getAccessToken(collection) {
    let sRealmApp;
    let time = new Date().getTime();

    if (collection === "users") {
      sRealmApp = "users-cntxf";
      if (time - accTokens.usersAccToken.time > 1798000) {
        let loginData = await mongoLogin(sRealmApp);
        let token = loginData.data.access_token;
        setAccTokens({
          ...accTokens,
          usersAccToken: { value: token, time: time },
        });
        return token;
      } else {
        return accTokens.usersAccToken.value;
      }
    } else if (collection === "faq") {
      sRealmApp = "faq-rwfci";
      if (time - accTokens.faqAccToken.time > 1798000) {
        let loginData = await mongoLogin(sRealmApp);
        let token = loginData.data.access_token;
        setAccTokens({
          ...accTokens,
          faqAccToken: { value: token, time: time },
        });
        return token;
      } else {
        return accTokens.faqAccToken.value;
      }
    } else {
      sRealmApp = "forumtopics-roiur";
      if (time - accTokens.forumAccToken.time > 1799000) {
        let loginData = await mongoLogin(sRealmApp);
        let token = loginData.data.access_token;
        setAccTokens({
          ...accTokens,
          forumAccToken: { value: token, time: time },
        });
        return token;
      } else {
        return accTokens.forumAccToken.value;
      }
    }
  }

  const data = { getAccessToken };
  return <AuthContext.Provider value={data}>{children} </AuthContext.Provider>;
};

export { AuthProvider, AuthContext };
