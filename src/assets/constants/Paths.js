export const BASENAME="/savjetovanje";
export const HOME="/";
export const ADVISOR_FORM="/advisorform";
export const PROFILE="/profile";
export const FAQ="/faq";
export const FORUM="/forum";
export const FORUM_TOPIC="/forum/:path";
export const REGISTER="/register";